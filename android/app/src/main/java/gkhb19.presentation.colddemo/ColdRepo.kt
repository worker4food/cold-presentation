package gkhb19.presentation.colddemo

import com.google.firebase.firestore.SetOptions
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.firestore.ktx.toObjects
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.tasks.asDeferred

class ColdRepo {

    suspend fun addCities() {
        listOf(
            City(
                id = "SF",
                name = "San Francisco",
                state = "CA",
                country = "USA",
                capital = false,
                population = 860_000,
                regions = listOf("west_coast", "norcal")
            ),
            City(
                id = "LA",
                name = "Los Angeles",
                state = "CA",
                country = "USA",
                capital = false,
                population = 3_900_000,
                regions = listOf("west_coast", "socal")
            ),
            City(
                id = "DC",
                name = "Washington D.C.",
                country = "USA",
                capital = true,
                population = 680_000,
                regions = listOf("east_coast")
            ),
            City(
                id = "TOK",
                name = "Tokyo",
                country = "Japan",
                capital = true,
                population = 9_000_000,
                regions = listOf("kanto", "honshu")
            ),
            City(
                id = "BJ",
                name = "Beijing",
                country = "China",
                capital = true,
                population = 21_500_000,
                regions = listOf("jingjinji", "hebei")
            )
        ).map {
            Firebase.firestore
                .collection(Collections.CITIES)
                .document(it.id!!)
                .set(it, SetOptions.merge())
                .asDeferred()
        }.awaitAll()
    }

    suspend fun addPlaces() {
        val citiesRef = Firebase.firestore
            .collection(Collections.CITIES)

        val sf = listOf(
            Place(
                name = "Golden Gate Bridge",
                type = PlaceTypes.Bridge
            ), Place(
                name = "Legion of Honor",
                type = PlaceTypes.Museum
            )
        ).map {
            citiesRef.document("SF")
                .collection(Collections.PLACES)
                .add(it)
                .asDeferred()
        }

        val la = listOf(
            Place(
                name = "Griffth Park",
                type = PlaceTypes.Park
            )
            , Place(
                name = "The Getty",
                type = PlaceTypes.Museum
            )
        ).map {
            citiesRef.document("LA")
                .collection(Collections.PLACES)
                .add(it)
                .asDeferred()
        }

        val dc = listOf(
            Place(
                name = "Lincoln Memorial",
                type = PlaceTypes.Memorial
            )
            , Place(
                name = "National Air and Space Museum",
                type = PlaceTypes.Museum
            )
        ).map {
            citiesRef.document("DC")
                .collection(Collections.PLACES)
                .add(it)
                .asDeferred()
        }

        val tok = listOf(
            Place(
                name = "Ueno Park",
                type = PlaceTypes.Park
            ), Place(
                name = "National Musuem of Nature and Science",
                type = PlaceTypes.Museum
            )
        ).map {
            citiesRef.document("TOK")
                .collection(Collections.PLACES)
                .add(it)
                .asDeferred()
        }

        val bj = listOf(
            Place(
                name = "Jingshan Park",
                type = PlaceTypes.Park
            )
            , Place(
                name = "Beijing Ancient Observatory",
                type = PlaceTypes.Museum
            )
        ).map {
            citiesRef.document("BJ")
                .collection(Collections.PLACES)
                .add(it)
                .asDeferred()
        }

        (sf + la + dc + tok + bj).awaitAll()
    }

    suspend fun addOrUpdateDocument(id: String): String {
        val ck = City(
            id = id,
            name = "Cherkasy",
            country = "Ukraine",
            capital = false,
            population = 275_000,
            regions = listOf("right_bank", "central")
        )

        Firebase.firestore
            .collection(Collections.CITIES)
            .document(id)
            .set(ck, SetOptions.merge())
            .await()

        return id
    }

    suspend fun getDocument(cityId: String): City? {
        val city1: City? = Firebase.firestore
            .collection(Collections.CITIES)
            .document(cityId)
            .get()
            .await()
            .toObject<City>()

        // Or with explicit path

        val city2: City? = Firebase.firestore
            .document("${Collections.CITIES}/$cityId")
            .get()
            .await()
            .toObject<City>()

        assert(city1 == city2)

        return city1
    }

    suspend fun deleteDocument(cityId: String) {
        Firebase.firestore
            .collection(Collections.CITIES)
            .document(cityId)
            .delete()
            .await()
    }

    suspend fun getCollection() =
        Firebase.firestore
            .collection(Collections.CITIES)
            .get()
            .await()
            .toObjects<City>()

    suspend fun getCollectionGroup() =
        Firebase.firestore
            .collectionGroup(Collections.PLACES)
            .get()
            .await()
            .toObjects<Place>()

    fun listenDocument(cityId: String) =
        Firebase.firestore
            .collection(Collections.CITIES)
            .document(cityId)
            .asFlow<City>()

    suspend fun simpleQuery1() =
        Firebase.firestore
            .collection(Collections.CITIES)
            .whereGreaterThanOrEqualTo("name", "San Francisco")
            .get()
            .await()
            .toObjects<City>()

    suspend fun simpleQuery2() =
        Firebase.firestore
            .collection(Collections.CITIES)
            .whereArrayContains("regions", "west_coast")
            .get()
            .await()
            .toObjects<City>()

    suspend fun compositeQuery1() =
        Firebase.firestore
            .collectionGroup(Collections.PLACES)
            .whereEqualTo("type", PlaceTypes.Museum)
            .whereGreaterThan("name", "National")
            .get()
            .await()
            .toObjects<Place>()

    suspend fun compositeQuery2() =
        Firebase.firestore
            .collection(Collections.CITIES)
            .whereEqualTo("state", "CA")
            .whereGreaterThan("population", 1_000_000)
            .get()
            .await()
            .toObjects<City>()
}
