package gkhb19.presentation.colddemo

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import timber.log.Timber

class MainActivity : AppCompatActivity(R.layout.activity_main) {
    private val repo = ColdRepo()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        fininshBtn.setOnClickListener {
            sendIntent(Const.STOP)
            finish()
        }

        lifecycleScope.launch {
            with(FirebaseAuth.getInstance()) {
                signInAnonymously().await()
                Timber.d("Add this ID to authors collection: ${currentUser?.uid}")
                Timber.d("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
            }

            sendIntent(Const.START)

            repo.addCities()
            //repo.addPlaces() //WARN

            // region Simple CRUD
//            val ckId = repo.addOrUpdateDocument("CK")
//            val ck = repo.getDocument(ckId)
//
//            Timber.d("$ck")
//
//            repo.deleteDocument(ckId)
            // endregion

            // region Get collection/collectionGroup
//            repo.getCollection().forEach {
//                Timber.d("${it.id}: ${it.name}")
//            }
//
//            repo.getCollectionGroup().forEach {
//                Timber.d("${it.id}: ${it.name} (${it.type})")
//            }
            // endregion

            // region Listen for changes
//            repo.listenDocument("DC").collect {
//                Timber.d("$it")
//            }
            // endregion

            // region Simple queries
//            Timber.d("name >= San Francisco:")
//            repo.simpleQuery1().forEach {
//                Timber.d("$it")
//            }
//
//            Timber.d("region contains west_coast:")
//            repo.simpleQuery2().forEach {
//                Timber.d("$it")
//            }
            // endregion

            // region Composite queries
//            Timber.d("Museums with name > National")
//            repo.compositeQuery1().forEach {
//                Timber.d("$it")
//            }
//
//            Timber.d("Cities with state = CA and population > 1 000 000")
//            repo.compositeQuery2().forEach {
//                Timber.d("$it")
//            }
            // endregion
        }

    }

    private fun sendIntent(act: String) {
        Intent(this, VolumeListenerSvc::class.java)
            .apply { action = act }
            .also {
                ContextCompat.startForegroundService(this, it)
            }
    }
}
