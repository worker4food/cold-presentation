package gkhb19.presentation.colddemo

import com.google.firebase.firestore.DocumentId

data class City(
    @DocumentId
    val id: String? = null,
    val name: String? = null,
    val state: String? = null,
    val country: String? = null,
    val capital: Boolean? = null,
    val population: Long? = null,
    val regions: List<String>? = null
)

data class Place(
    @DocumentId
    val id: String? = null,
    val name: String? = null,
    val type: PlaceTypes? = null
)

enum class PlaceTypes {
    Museum,
    Park,
    Bridge,
    Memorial
}