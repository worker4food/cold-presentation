package gkhb19.presentation.colddemo

import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.firestore.ktx.toObjects
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

@OptIn(ExperimentalCoroutinesApi::class)
inline fun <reified T: Any> Query.asFlow(): Flow<List<T>> = callbackFlow {
    val listener = addSnapshotListener { snapshot, ex ->
        if(ex != null) close(ex)
        else snapshot
            ?.toObjects<T>()
            ?.also { offer(it) }
    }

    awaitClose { listener.remove() }
}

@OptIn(ExperimentalCoroutinesApi::class)
inline fun <reified T> DocumentReference.asFlow(): Flow<T> = callbackFlow {
    val listener = addSnapshotListener { snapshot, ex ->
        if(ex != null) close(ex)
        else snapshot
            ?.toObject<T>()
            ?.also { offer(it) }
    }

    awaitClose { listener.remove() }
}

