package gkhb19.presentation.colddemo

import android.app.*
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.support.v4.media.session.MediaSessionCompat
import android.support.v4.media.session.PlaybackStateCompat
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.content.getSystemService
import androidx.media.VolumeProviderCompat
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.tasks.await
import timber.log.Timber

const val CHANNEL_ID = "VLS"
const val SERVICE_NOTIFICATION_ID = 42

class VolumeListenerSvc : Service() {

    private val thisScope = CoroutineScope(Dispatchers.IO)
    private val counterRef by lazy { Firebase.firestore.document("presentation/counter") }

    private val mediaSession by lazy {
        MediaSessionCompat(this, "PlayerService").apply {
            setFlags(
                MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS or
                        MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS
            )

            PlaybackStateCompat.Builder()
                .setState(PlaybackStateCompat.STATE_PLAYING, 0, 0F)
                .build()
                .also(::setPlaybackState)
        }
    }

    private val channelId by lazy {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                CHANNEL_ID,
                "$CHANNEL_ID for service #$SERVICE_NOTIFICATION_ID",
                NotificationManager.IMPORTANCE_LOW
            )

            getSystemService<NotificationManager>()?.createNotificationChannel(channel)

            channel.id
        } else ""
    }

    override fun onBind(intent: Intent?): IBinder? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent?.action == Const.START) {
            startForeground(SERVICE_NOTIFICATION_ID, mkNotification("Initializing..."))
            startMe()
        } else {
            stopForeground(true)
            stopSelf()
        }

        return START_NOT_STICKY
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    private fun startMe() = thisScope.launch {
        FirebaseAuth.getInstance().signInAnonymously().await()

        val volumeProvider = mkVolumeProvider(readCounter())

        withContext(Dispatchers.Main) {
            mediaSession.setPlaybackToRemote(volumeProvider)
        }
        mediaSession.isActive = true

        counterRef.asFlow<Counter>().collect {
            getSystemService<NotificationManager>()
                ?.notify(SERVICE_NOTIFICATION_ID, mkNotification(it))

            volumeProvider.currentVolume = it.slide
        }
    }

    private suspend fun readCounter(): Counter =
        counterRef.get().await().toObject<Counter>()!!

    private fun updateCounterVal(fn: (Int) -> Int) = thisScope.launch {
        val (currentValue, maxValue) = readCounter()
        val newValue = fn(currentValue).coerceIn(1, maxValue)

        Counter(newValue, maxValue).also {
            counterRef.set(it).await()
        }
    }

    override fun onDestroy() {
        mediaSession.release()
        thisScope.cancel("onDestroy")
        super.onDestroy()
    }

    private fun mkNotification(counter: Counter) =
        mkNotification("Slide #${counter.slide} of ${counter.totalSlides}")

    private fun mkNotification(text: String): Notification {

        val pendingIntent = PendingIntent.getActivity(
            this,
            SERVICE_NOTIFICATION_ID,
            Intent(this, MainActivity::class.java),
            0
        )

        return NotificationCompat.Builder(this, channelId)
            .setContentTitle("Presentation control")
            .setContentText(text)
            .setSmallIcon(R.drawable.ic_hearing_black)
            .setContentIntent(pendingIntent)
            .build()
    }

    private fun mkVolumeProvider(counter: Counter) = VolumeProvider(counter)

    inner class VolumeProvider(counter: Counter) : VolumeProviderCompat(
        VOLUME_CONTROL_ABSOLUTE,
        counter.totalSlides,
        counter.slide
    ) {
        override fun onAdjustVolume(direction: Int) {
            if (direction == 1) updateCounterVal(Int::dec)
            else if (direction == -1) updateCounterVal(Int::inc)
        }
    }
}
