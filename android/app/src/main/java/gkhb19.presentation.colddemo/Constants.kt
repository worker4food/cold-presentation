package gkhb19.presentation.colddemo

object Const {
    const val START = "DO IT"
    const val STOP = "STOP DOING IT"
}

object Collections {
    const val CITIES = "cities"
    const val PLACES = "places"
}